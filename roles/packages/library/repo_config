#!/usr/bin/python3

#######################################################################################################################
# DESCRIPTION
#

'''
Module for Ansible: repo_config.
It updates some files in /etc/yum.repos.d/ directory of the local host.
Arguments to provide:
- snapdate: formatted as 'YYYY-MM-DD' or 'canary'
- repos: dictionary of constants defining repos
Files touched:
- /etc/dnf/vars/{cernstream8,epelbase}
- /etc/yum.repos.d/*.repo
'''

#######################################################################################################################
# IMPORTS
#

import json
import os
import re
import requests
import sys
import yaml

#######################################################################################################################
# FUNCTIONS
#

#----------------------------------------------------------------------------------------------------------------------
def add_missing_lines(prev_enabled_done, prev_protect_done, prev_priority_done):
    '''
    Add missing lines to finishing section.
    '''
    return_str = ''

    # Add new 'enabled=' line if not present yet in previous section
    if not prev_enabled_done:
        if current_section in enabled_repo_set:
            return_str += 'enabled=1\n'
        else:
            return_str += 'enabled=0\n'

    # Add new 'protect=' line if not present yet in previous section
    if not prev_protect_done:
        if current_section in enabled_repo_set or current_section == 'acc-external-do':
            return_str += 'protect=1\n'
        else:
            return_str += 'protect=0\n'

    # Add new 'priority=' line if not present yet in previous section
    if not prev_priority_done:
        if current_section == 'acc-external-do':
            return_str += 'priority=5\n'
        elif current_section in enabled_repo_set:
            return_str += 'priority=7\n'
        else:
            return_str += 'priority=10\n'

    return return_str

#######################################################################################################################
# PARSE ARGUMENTS AND PREPARE ANSIBLE OUTPUT
#

# Ansible guarantees to provide a file containing JSON arguments
# WANT_JSON : This word tells Ansible to provide arguments in JSON format
with open(sys.argv[1], 'r') as args_stream:
    args_dict = json.load(args_stream)

# Check module arguments
if 'snapdate' not in args_dict \
or 'repos' not in args_dict:
    print(json.dumps({'failed': True, 'msg': 'Missing module arguments'}))
    sys.exit(1)

# Simplify access to arguments
snapdate = args_dict['snapdate']
repo_versions = args_dict['repos']['versions']
enabled_repos = args_dict['repos']['enabled_repos']
url = args_dict['repos']['url']

#######################################################################################################################
# UPDATE YUM REPOSITORY CONFIG FILES
#

#----------------------------------------------------------------------------------------------------------------------
# UPDATE DNF VARIABLE ($cernstream8 and $epelbase)
#

# Define variables
if snapdate == 'canary':
    new_cernstream8 = 's8'
    new_epelbase = ''
else:
    for version_date in sorted(repo_versions.keys())[::-1]:
        if snapdate >= version_date:
            new_cernstream8 = repo_versions[version_date]
            break
    new_epelbase = 'snapshots/' + snapdate.replace('-', '')

# Write cernstream8 variable to files
with open('/etc/dnf/vars/cernstream8', 'r') as dnf_var_stream:
    cur_cernstream8 = dnf_var_stream.read()
if new_cernstream8 != cur_cernstream8:
    with open('/etc/dnf/vars/cernstream8', 'w') as cernstream8_stream:
        cernstream8_stream.write(new_cernstream8 + '\n')

# Write epelbase variable to files
with open('/etc/dnf/vars/epelbase', 'r') as epelbase_stream:
    cur_epelbase = epelbase_stream.read()
if new_epelbase != cur_epelbase:
    with open('/etc/dnf/vars/epelbase', 'w') as epelbase_stream:
        epelbase_stream.write(new_epelbase + '\n')

#----------------------------------------------------------------------------------------------------------------------
# UPDATE YUM REPOS
#

# Build set of enabled repos
enabled_repo_set = set(enabled_repos)

# Build list of repo files
repo_dir = '/etc/yum.repos.d/'
filename_list = [ f for f in os.listdir(repo_dir) if os.path.isfile(os.path.join(repo_dir, f)) ]

# Format snapdate (when not canary)
if snapdate != 'canary':
    formatted_snapdate = snapdate.replace('-', '')

# Scan files in repo dir
ansible_state = 'ok'
ansible_change_list = list()
for filename in filename_list:

    # Only keep *.repo files
    if not re.search('\.repo$', filename):
        continue

    # Load repo file
    repo_path = os.path.join(repo_dir, filename)
    with open(repo_path, 'r') as repo_stream:
        current_repo_str = repo_stream.read().rstrip()

    # Scan current repo string line by line and create new repo string
    new_repo_str = ''
    current_section = ''
    prev_enabled_done = True
    prev_protect_done = True
    prev_priority_done = True
    for current_line in current_repo_str.splitlines():

        # Remove empty lines
        if not current_line:
            continue

        # Search for section name
        elif current_line and current_line[0] == '[':

            # Add missing lines to previous section
            new_repo_str = new_repo_str.rstrip() + '\n'
            new_repo_str += add_missing_lines(prev_enabled_done, prev_protect_done, prev_priority_done)
            new_repo_str += '\n'

            # Set new section
            current_section = re.sub('\[|\].*', '', current_line)
            new_line = current_line
            prev_enabled_done = False
            prev_protect_done = False
            prev_priority_done = False

        # Force 'enabled=' line
        elif re.match('enabled=', current_line):
            if current_section in enabled_repo_set:
                new_line = 'enabled=1'
            else:
                new_line = 'enabled=0'
            prev_enabled_done = True

        # Force 'protect=' line (protect=1: packages protected from repos where protect=0)
        elif re.match('protect=', current_line):
            if current_section in enabled_repo_set or current_section == 'acc-sysadm':
                new_line = 'protect=1'
            else:
                new_line = 'protect=0'
            prev_protect_done = True

        # Force 'priority=' line (priority=99: lowest, priority=1: highest)
        elif re.match('priority=', current_line):
            if current_section == 'acc-sysadm':
                new_line = 'priority=5'
            elif current_section in enabled_repo_set:
                new_line = 'priority=7'
            else:
                new_line = 'priority=10'
            prev_priority_done = True

        # Force 'baseurl=' line
        elif re.match('baseurl', current_line):
            if filename in url and current_section in url[filename]:
                new_line = 'baseurl=' + url[filename][current_section]
            else:
                new_line = current_line

        # Otherwise just copy the line
        else:
            new_line = current_line

        # Add new line to new repo
        new_repo_str += new_line + '\n'

    # Add missing lines to close last section
    new_repo_str += add_missing_lines(prev_enabled_done, prev_protect_done, prev_priority_done)

    # Update repo if it has changed
    if new_repo_str.rstrip() != current_repo_str:
        ansible_state = 'changed'
        ansible_change_list.append(filename)
        with open(repo_path, 'w') as repo_stream:
            repo_stream.write(new_repo_str)

#######################################################################################################################
# RETURN ANSIBLE VALUES
#

print(json.dumps({ansible_state: True, 'changed_repos': sorted(ansible_change_list)}))
sys.exit(0)

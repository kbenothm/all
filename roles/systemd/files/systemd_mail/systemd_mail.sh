#!/bin/bash
# File managed by ansible

/usr/sbin/sendmail -t << SYSTEMD_MAIL
To: root@localhost
From: systemd <root@$HOSTNAME>
Subject: $1
Content-Transfer-Encoding: 8bit
Content-Type: text/plain; charset=UTF-8

$(journalctl -u $1 -an 10)
SYSTEMD_MAIL

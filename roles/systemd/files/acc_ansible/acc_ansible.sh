#!/bin/bash
# /etc/systemd/scripts/acc_ansible.sh
# File managed by ansible

# Run acc_ansible_pull and save log in /var/log/acc_playbook.log
/usr/local/sbin/acc_ansible_cs8 -r &> /var/log/acc_ansible.log

# Create remote destination on cs-ccr-accadm
dest_dir=/nfs/cs-ccr-accadm/srv/log/$(hostname -s)
[[ ! -d $dest_dir ]] && mkdir -p $dest_dir

# Copy log to remote 
cp /var/log/acc_ansible.log $dest_dir/acc-daily-playbook.log

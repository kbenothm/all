# /etc/auto.dsc
# File managed by ansible

# Subdirectories of /acc/dsc
# sub-dir   mount-options   location
lab                         cs-ccr-felab:/data/dsc/lab
tst                         cs-ccr-felab:/data/dsc/tst
lhc                         cs-ccr-felhc:/data/dsclhc/lhc
oplhc                       cs-ccr-felhc:/data/dsclhc/oplhc
svc                         cs-ccr-fe365:/data/dscsvc/svc
opsvc                       cs-ccr-fe365:/data/dscsvc/opsvc
src                         cs-ccr-nfsop:/srv/nfs1/vol30/src/dsc
*                           cs-ccr-feop:/data/dsc/&

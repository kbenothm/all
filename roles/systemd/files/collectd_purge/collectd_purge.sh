#!/bin/bash

# /etc/systemd/scripts/collectd_purge.sh
# File manage by ansible

# Archive collectd RRD files for old hosts,
# and remove their InfluxDB data.
# Remove inactive partitions.
# Last edit 2019-03


# Global variables #

PROGNAME=$0
DAYS_OLD=1
RRD_DIR=/opt/collectd/rrd/
RRD_ARCHIVE_DIR=/opt/collectd/rrd-archive/


# Main #

# Check whether localhost is a COSMOS server
if [[ ! -d $RRD_DIR ]]
then
    echo "$RRD_DIR doesn't exist, exiting..."
    exit 1
fi
cd $RRD_DIR > /dev/null

# HOST ARCHIVAL AND REMOVAL #
# Create RRD archive directory
[[ ! -d $RRD_ARCHIVE_DIR ]] && mkdir -p $RRD_ARCHIVE_DIR

# Get list of hosts for which RRD files haven't been updated for more than
# defined set of days
old_hosts=$(find . -name '*.rrd' -mtime +$DAYS_OLD | cut -d '/' -f 2 | sort -u)

# If the host is not in the DNS, archive it and purge its InfluxDB data
for h in $old_hosts
do
    if ! gethostip $h &> /dev/null
    then
        echo "Archiving RRD files for $h in $RRD_ARCHIVE_DIR..."
        mv $h $RRD_ARCHIVE_DIR
        echo "Purging InfluxDB data for $h..."
        source /acc/local/share/python/acc-py/setup.sh
        /opt/virtualenvs/influxdb/bin/python /opt/cosmos/scripts/cleanuphosts -H $h
    fi
done

# PURGE INACTIVE PARTITIONS #
# Get list of partitions for which the free space RRD file hasn't been updated for more than
# defined set of days
obsolete_df_rrds=$(find . -path "./*/df-*/df_complex-free.rrd" -mtime +$DAYS_OLD)

for obsolete_df_rrd in $obsolete_df_rrds
do
    # Check if other partitions for the same host have changed
    # in the same interval
    host_to_analyse=$(echo $obsolete_df_rrd | cut -d '/' -f 2)
    updated_partitions=$(find . -path "./${host_to_analyse}/df-*/df_complex-free.rrd" -mtime -${DAYS_OLD})

    # If other partitions for the host have been updated, then
    # we need to delete the inactive partition
    # If other partitions haven't been updated, the host is
    # probably shut down: no deletion should occur
    if [[ -n $updated_partitions ]]
    then
        to_del=$(realpath $(dirname $obsolete_df_rrd))
        # Sanity checking the directory to delete
        if [[ $to_del =~ $RRD_DIR ]]
        then
            rm -fr $to_del
            echo "Deleted RRD for unused partition: ${to_del}"
        else
            echo "ERROR: Attempted to delete ${to_del}, didn't pass the sanity check!"
        fi
    fi
done

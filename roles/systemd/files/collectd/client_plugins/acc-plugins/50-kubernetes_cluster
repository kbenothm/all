#!/bin/bash

# /etc/collectd.d/acc-plugins/50-kubernetes_cluster
# File managed by ansible

# Provide Kubernetes cluster metrics to collectd.
# To be executed on a Kubernetes master only!
# Metrics:
# - number of pods for every worker of the cluster
# - CPU percentage for every worker of the cluster
# - memory percentage for every worker of the cluster
# 2018-09

set -e


# Global variables #

HOSTNAME="${COLLECTD_HOSTNAME:-localhost}"
INTERVAL=300
PLUGIN_INSTANCE="kubernetes_cluster"

# Kubernetes specific variable
export KUBECONFIG='/etc/kubernetes/admin.conf'


# Functions #

# Example output:
#      2 cs-ccr-kubedock1.cern.ch
#      2 cs-ccr-kubedock2.cern.ch
#      2 cs-ccr-kubedock3.cern.ch
#      2 cs-ccr-kubedock4.cern.ch
#     10 cs-ccr-kubedock5.cern.ch
kubernetes_get_podsperworker() {
	sudo /bin/kubectl get pod -o=custom-columns=NODE:.spec.nodeName --all-namespaces | grep -v ^NODE | sort | uniq -c
}

# Example output:
# cs-ccr-kubedock1.cern.ch 52m 0% 3739Mi 2%
# cs-ccr-kubedock2.cern.ch 47m 0% 3717Mi 2%
# cs-ccr-kubedock3.cern.ch 50m 0% 3756Mi 2%
# cs-ccr-kubedock4.cern.ch 52m 0% 3820Mi 2%
# cs-ccr-kubedock5.cern.ch 290m 0% 4223Mi 3%
kubernetes_get_workersstats() {
	sudo /bin/kubectl top nodes | grep -v ^NAME | tr -s " "
}


# Main #

while : ; do
	# Get and display number of pods per Kubernetes worker
	podsperworker=$(kubernetes_get_podsperworker)
	while read line ; do
		podsnum=$(echo $line | cut -d " " -f 1)
		workername=$(echo $line | cut -d " " -f 2)
		# Empty workername: do not output anything
		[[ -z "${workername}" ]] && continue
		echo "PUTVAL \"${workername}/exec-${PLUGIN_INSTANCE}/gauge-kubernetes_pods\" interval=$INTERVAL N:$podsnum"
	done <<< "$podsperworker"

	# Get CPU and RAM usage per Kubernetes worker
	workersstats=$(kubernetes_get_workersstats)
	while read line ; do
		workername=$(echo $line | cut -d " " -f 1)
		cpupercent=$(echo $line | cut -d " " -f 3 | grep -o '[0-9]')
		mempercent=$(echo $line | cut -d " " -f 5 | grep -o '[0-9]')
		# Empty workername: do not output anything
		[[ -z "${workername}" ]] && continue
		echo "PUTVAL \"${workername}/exec-${PLUGIN_INSTANCE}/percent-kubernetes_cpu\" interval=$INTERVAL N:$cpupercent"
		echo "PUTVAL \"${workername}/exec-${PLUGIN_INSTANCE}/percent-kubernetes_memory\" interval=$INTERVAL N:$mempercent"
	done <<< "$workersstats"
	sleep "$INTERVAL"
done

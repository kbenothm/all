# /root/.bashrc
# File managed by ansible

# Always define this
export LANG='C'
export KRB5CCNAME='/tmp/krb5cc_host'
export ANSIBLE_CONFIG='/opt/ansible/ansible.cfg'
export HISTTIMEFORMAT='%F %T  '

# If not interactive shell, stop here
[[ $- == *i* ]] || return

# Add some aliases
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

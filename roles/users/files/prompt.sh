#!/bin/bash

# /etc/profile.d/prompt.sh
# File managed by ansible

export PS1='\[\e[44;1;37m\][\u@\h]\[\e[00m\] \w \$ '

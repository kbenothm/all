#!/usr/bin/python3

#######################################################################################################################
# DESCRIPTION
#

'''
Module for Ansible: update_nfs_homes.
This module:
  - creates directories when localhost hosts NFS homes (on NFS servers),
  - updates /user symlinks (on all hosts),
Arguments:
  - nfs_homes: NFS home directories.
  - op_env: operational environment.
Files accessed:
  - /user/<username>
  - <nfs_home_dirs> if localhost is an NFS server
'''

#######################################################################################################################
# IMPORTS
#

import grp
import json
import os
import pwd
import re
import shutil
import subprocess
import sys
import yaml

#######################################################################################################################
# FUNCTIONS
#

#----------------------------------------------------------------------------------------------------------------------
def run_system_cmd(cmd, raise_error=True):
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    (cmd_output, cmd_error) = process.communicate()
    if raise_error and process.returncode != 0:
        print(json.dumps({'failed': True, 'msg': cmd_error.decode()}))
        sys.exit(1)
    return cmd_output.decode()

#######################################################################################################################
# PROGRAM
#

#----------------------------------------------------------------------------------------------------------------------
# INIT VARS
#

NFS_HOME_EGROUP = 'ACC-nfsdev-home'
DEFAULT_NFS_BASE = '/nfs/cs-ccr-nfshome/user'
USER_SYMLINKS_BASE = '/user'
ansible_changed = False
ansible_msg_list = list()

#----------------------------------------------------------------------------------------------------------------------
# GET ANSIBLE ARGUMENTS
#

# Ansible guarantees to provide a file containing JSON arguments
# WANT_JSON : This word tells Ansible to provide arguments in JSON format
with open(sys.argv[1], 'r') as args_stream:
    args_dict = json.load(args_stream)

# Check module arguments
if 'nfs_homes' not in args_dict \
or 'op_env' not in args_dict:
    print(json.dumps({'failed': True, 'msg': 'Missing module arguments'}))
    sys.exit(1)

#----------------------------------------------------------------------------------------------------------------------
# CREATE HOME DIRECTORIES WHEN LOCALHOST IS AN NFS SERVER
# (we suppose the NFS export is RW in order to create directories through it)
#

# Get users from NFS_HOME_EGROUP
try:
    nfs_egroup_list = grp.getgrnam(NFS_HOME_EGROUP)[3]
except:
    nfs_egroup_list = list()

# Merge users from NFS_HOME_EGROUP with those statically defined (nfs_homes)
nfs_link_dict = args_dict['nfs_homes']
for username in nfs_egroup_list:
    if username not in nfs_link_dict:
        nfs_link_dict[username] = DEFAULT_NFS_BASE + '/' + username

# Scan all NFS homes that should exist
for username, nfs_home in nfs_link_dict.items():
    ansible_msg = ''

    # Skip NFS home if not on localhost (home path is supposed to be /nfs/<hostname>/...)
    field_list = nfs_home.split('/')
    if field_list[1] != 'nfs':
        continue
    output = run_system_cmd('df ' + re.sub('/[^/]*$', '', nfs_home))
    if not re.match('/dev/', output.splitlines()[1]):
        continue

    # Rename in .old if NFS home exists but is not a directory
    if os.path.exists(nfs_home) and not os.path.isdir(nfs_home):
        os.rename(nfs_home, nfs_home + '.old')
        ansible_msg = ' Moved ' + nfs_home + ' -> ' + nfs_home + '.old'

    # If NFS home does not exist yet
    if not os.path.exists(nfs_home):
        ansible_changed = True
        ansible_msg = ' Created ' + nfs_home

        # Get uid and git of owner
        try:
            pwd_line = pwd.getpwnam(username)
            uid = pwd_line[2]
            gid = pwd_line[3]
        except:
            uid = 0
            gid = 0

        # Create home dir
        os.makedirs(nfs_home, mode=0o755)
        os.chown(nfs_home, uid, gid)

        # Create subdirectories when op user and provided subdirs
        if username in op_env_dict and 'shared_directories' in op_env_dict[username]:
            for subdir in op_env_dict[username]['shared_directories']:
                subdir_path = nfs_home + '/' + subdir
                ansible_msg = ' Created ' + subdir_path
                os.makedirs(subdir_path)
                os.chown(subdir_path, uid, gid)

    if ansible_msg:
        ansible_msg_list.append(ansible_msg)

#----------------------------------------------------------------------------------------------------------------------
# CREATE /user DIRECTORY
#

if os.path.exists(USER_SYMLINKS_BASE) and not os.path.isdir(USER_SYMLINKS_BASE):
    os.rename(USER_SYMLINKS_BASE, USER_SYMLINKS_BASE + '.old')
if not os.path.exists(USER_SYMLINKS_BASE):
    os.mkdir(USER_SYMLINKS_BASE)

#----------------------------------------------------------------------------------------------------------------------
# REMOVE UNKNOWN AND INCORRECT SYMLINKS, DIRECTORIES AND FILES IN /user DIRECTORY
#

# Get all entries in /user directory (use system command to avoid mounting NFS with Python equivalent)
output = run_system_cmd('ls -Al --color=never ' + USER_SYMLINKS_BASE)

# Scan each line of the system command
for line in output.splitlines():

    # Skip not understood lines
    if not re.match('[-bcCdDlMnpPs?]', line):
        continue

    # Delete unknown and incorrect links
    if line[0] == 'l':
        link_name = line.split()[-3]
        link_target = line.split()[-1]
        if (link_name not in nfs_link_dict or link_target != nfs_link_dict[link_name]):
            link_path = USER_SYMLINKS_BASE + '/' + link_name
            os.unlink(link_path)
            ansible_changed = True
            ansible_msg_list.append('Removed symlink \'' + link_path + '\' -> \'' + link_target + '\'')

    # Delete all directories in /user
    elif line[0] == 'd':
        dir_path = USER_SYMLINKS_BASE + '/' + line.split()[-1]
        shutil.rmtree(dir_path)
        ansible_changed = True
        ansible_msg_list.append('Removed directory \'' + dir_path + '\'')

    # Delete all files in /user
    else:
        file_path = USER_SYMLINKS_BASE + '/' + line.split()[-1]
        os.remove(file_path)
        ansible_changed = True
        ansible_msg_list.append('Removed file \'' + file_path + '\'')

#----------------------------------------------------------------------------------------------------------------------
# ADD REQUESTED SYMLINKS in /user DIRECTORY
#

# Scan all nfs homes
for username, link_target in nfs_link_dict.items():
    link_path = USER_SYMLINKS_BASE + '/' + username

    # Create symlink to nfs home if it does not exist yet
    if not os.path.lexists(link_path):
        os.symlink(link_target, link_path)
        ansible_changed = True
        ansible_msg_list.append('Created symlink \'' + link_path + '\' -> \'' + link_target + '\'')

#----------------------------------------------------------------------------------------------------------------------
# RETURN ANSIBLE VALUES
#

print(json.dumps({'ok': True, 'changed': ansible_changed, 'msgs': ansible_msg_list }))
sys.exit(0)

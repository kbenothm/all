#!/usr/bin/python3

#######################################################################################################################
# DESCRIPTION
#

'''
Module for Ansible: update_groups.
It updates /etc/group and /etc/gshadow of the local host.
The script accesses LDAP service on cerndc or tndc.
Arguments to provide:
- calculated_groups:    { group_1: { ... }, ... }
- ldap_groups:          { egroup_1: { ... }, ... }
- specialist_gid:       gid
- specialist_egroups:   { egroup_1: { ... }, ... }
- static_groups:        { group_1: { ... }, ... }     # optional argument
- ldap_srv:             cerndc.cern.ch or tndc.cern.ch
- admin_users:          [ admin_user_1, ... ]
- main_users:           [ main_user_1, ... ]
Accessed files:
- /etc/group     (read/write)
- /etc/gshadow   (write)
'''

#######################################################################################################################
# IMPORTS
#

import json
import ldap3
import os
import pwd
import re
import ssl
import sys
import subprocess
import yaml

#######################################################################################################################
# FUNCTIONS
#

#----------------------------------------------------------------------------------------------------------------------
def egroup_user_set(egroup_str):
    '''
    Recursive function returning a set of users of an egroup, extracted from LDAP
    '''
    try:
        ldap_conn.search('ou=e-groups,ou=workgroups,dc=cern,dc=ch', '(cn=' + egroup_str + ')', attributes=['member'])
    except:
        print(json.dumps({'failed' : True, 'msg': 'failed to request e-groups to Active Directory'}))
        sys.exit(1)
    user_set = set()
    if len(ldap_conn.entries) > 0 and 'member' in ldap_conn.entries[0]:
        for member_str in ldap_conn.entries[0]['member']:
            field_list = re.split('[=,]', member_str)
            if field_list[3] == 'Users':
                user_set.add(field_list[1])
            else:
                user_set |= egroup_user_set(field_list[1])
    return user_set

#######################################################################################################################
# PROGRAM
#

#----------------------------------------------------------------------------------------------------------------------
# GET ANSIBLE ARGUMENTS
#

# Ansible guarantees to provide a file containing JSON arguments
# WANT_JSON : This word tells Ansible to provide arguments in JSON format
with open(sys.argv[1], 'r') as args_stream:
    args_dict = json.load(args_stream)

# Check module arguments
if 'calculated_groups' not in args_dict \
or 'ldap_groups' not in args_dict \
or 'specialist_gid' not in args_dict \
or 'specialist_egroups' not in args_dict \
or 'ldap_srv' not in args_dict \
or 'admin_users' not in args_dict \
or 'main_users' not in args_dict:
    print(json.dumps({'failed': True, 'msg': 'Missing module arguments'}))
    sys.exit(1)

#----------------------------------------------------------------------------------------------------------------------
# INITIALIZE AUTHENTICATED CONNECTION TO ACTIVE DIRECTORY SERVER THROUGH LDAP
#

# Initialize authenticated ldap connection
os.environ['KRB5CCNAME'] = '/tmp/krb5cc_host'
srv_uri = args_dict['ldap_srv']
tls = ldap3.Tls(validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLSv1_2)
ldap_srv = ldap3.Server(srv_uri, use_ssl=True, tls=tls)
ldap_conn = ldap3.Connection(ldap_srv, authentication=ldap3.SASL, sasl_mechanism=ldap3.KERBEROS, sasl_credentials=(True,))

# Bind connection
try:
    ldap_conn.bind()
except:
    print(json.dumps({'failed' : True, 'msg' : 'Failed biding to AD server: ' + ldap_srv}))
    sys.exit(1)

#----------------------------------------------------------------------------------------------------------------------
# GET ALL AFS GROUPS FROM ACTIVE DIRECTORY (1/6)
#

# Make request
try:
    ldap_conn.search('ou=unix,ou=workgroups,dc=cern,dc=ch', '(cn=*)', attributes=['cn', 'gidNumber'])
except:
    print(json.dumps({'failed' : True, 'msg': 'failed requesting UNIX groups to Active Directory'}))
    sys.exit(1)

# Format answer
all_group_dict = dict()
for afs_group_info in ldap_conn.entries:
    if 'cn' in afs_group_info and 'gidNumber' in afs_group_info:
        afs_group_str = str(afs_group_info['cn']) + '::' + str(afs_group_info['gidNumber']) + ':\n'
        all_group_dict[str(afs_group_info['gidNumber'])] = afs_group_str

#----------------------------------------------------------------------------------------------------------------------
# GET E-GROUPS FOR SPECIALISTS OF LOCAL HOST (2/6)
#

users_set = set()
specialist_gid = args_dict['specialist_gid']
for egroup_name in args_dict['specialist_egroups'].keys():
    users_set |= egroup_user_set(egroup_name)
users_str = ','.join(sorted(users_set))
group_str = 'specialist::' + str(specialist_gid) + ':'
if users_set:
    group_str += ','.join(sorted(users_set))
all_group_dict[str(specialist_gid)] = group_str + '\n'

#----------------------------------------------------------------------------------------------------------------------
# GET E-GROUPS: NAMES FROM VARS, USERNAMES FROM ACTIVE DIRECTORY (3/6)
#

for group_name, group_dict in args_dict['ldap_groups'].items():
    if 'egroup_name' in group_dict and group_dict['egroup_name']:
        egroup_name = group_dict['egroup_name']
    else:
        egroup_name = group_name
    users_set = egroup_user_set(egroup_name)
    if 'nonafs_users' in group_dict and group_dict['nonafs_users']:
        users_set |= set(group_dict['nonafs_users'])
    users_str = ','.join(sorted(users_set))
    group_str = group_name + '::' + str(group_dict['gid']) + ':'
    if users_set:
        group_str += ','.join(sorted(users_set))
    all_group_dict[str(group_dict['gid'])] = group_str + '\n'

#----------------------------------------------------------------------------------------------------------------------
# CLOSE CONNECTION TO LDAP SERVER
#

ldap_conn.unbind()

#----------------------------------------------------------------------------------------------------------------------
# GET SYSTEM GROUPS FROM /etc/group FILE (4/6)
#

SYSTEM_GROUP_PATH = '/etc/group'
SYSTEM_GSHADOW_PATH = '/etc/gshadow'
if not os.path.isfile(SYSTEM_GROUP_PATH):
    print(json.dumps({'failed' : True, 'msg': SYSTEM_GROUP_PATH + ' file not found'}))
    sys.exit(1)
group_file = open(SYSTEM_GROUP_PATH, 'r')
cur_group_str = group_file.read()
group_file.close()
for line_str in cur_group_str.split('\n'):
    if line_str:
        field_list = line_str.split(':')
        gid = field_list[2]
        if int(gid) < 1000 or gid == '65534':
            all_group_dict[gid] = line_str + '\n'

#----------------------------------------------------------------------------------------------------------------------
# GET STATIC GROUPS: FROM VARS, IF ANY, AND IF NOT ALREADY PRESENT (5/6)
#

if 'static_groups' in args_dict:
    for group_name, group_dict in args_dict['static_groups'].items():
        if not 'gid' in group_dict or not group_dict['gid']:
            print(json.dumps({'failed' : True, 'msg': 'group var not correctly defined'}))
            sys.exit(1)
        if group_name in group_dict:
            continue
        if 'users' not in group_dict or not group_dict['users']:
            users_list = list()
        else:
            users_list = sorted(group_dict['users'])
        all_group_dict[str(group_dict['gid'])] = group_name + '::' + str(group_dict['gid']) + ':' + ','.join(users_list) + '\n'

#----------------------------------------------------------------------------------------------------------------------
# CALCULATE GROUPS: DIFFERENT ON EACH HOST (6/6)
#

# Warning (SLC5): u'username' -> username, for dynamic Ansible groups
LANDBADMIN_NAME = 'landbadmin'
LANDBMAINUSER_NAME = 'landbmainuser'
for group_name, group_dict in args_dict['calculated_groups'].items():
    if group_name == LANDBADMIN_NAME:
        all_group_dict[str(group_dict['gid'])] = group_name + '::' + str(group_dict['gid']) + ':' \
        + re.sub('u\'|\'', '', ','.join(args_dict['admin_users'])) + '\n'
    elif group_name == LANDBMAINUSER_NAME:
        all_group_dict[str(group_dict['gid'])] = group_name + '::' + str(group_dict['gid']) + ':' \
        + re.sub('u\'|\'', '', ','.join(args_dict['main_users'])) + '\n'
    else:
        print(json.dumps({'failed' : True, 'msg': 'failed: unknown calculated group ' + group['group_name']}))
        sys.exit(1)

#----------------------------------------------------------------------------------------------------------------------
# UPDATE LOCAL FILES
#

# Format new group and gshadow strings
new_group_str = ''
new_gshadow_str = ''
for gid in sorted(all_group_dict, key=int):
    new_group_str += all_group_dict[gid]
    fields_list = all_group_dict[gid].strip().split(':')
    new_gshadow_str += fields_list[0] + ':::'
    if fields_list[3]:
        new_gshadow_str += fields_list[3]
    new_gshadow_str += '\n'

# Update local files when necessary
if cur_group_str != new_group_str:
    ansible_changed = True
    os.rename(SYSTEM_GROUP_PATH, SYSTEM_GROUP_PATH + '.old')
    group_file = open(SYSTEM_GROUP_PATH, 'w')
    group_file.write(new_group_str)
    group_file.close()
    os.rename(SYSTEM_GSHADOW_PATH, SYSTEM_GSHADOW_PATH + '.old')
    gshadow_file = open(SYSTEM_GSHADOW_PATH, 'w')
    gshadow_file.write(new_gshadow_str)
    gshadow_file.close()
else:
    ansible_changed = False

# Restrict gshadow access (root:root, mode: 600)
os.chown(SYSTEM_GSHADOW_PATH, 0, 0)
os.chmod(SYSTEM_GSHADOW_PATH, 384)

#----------------------------------------------------------------------------------------------------------------------
# RETURN ANSIBLE VALUES
#

print(json.dumps({'ok': True, 'changed': ansible_changed }))
sys.exit(0)

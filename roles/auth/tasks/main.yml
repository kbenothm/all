- name: Configure files (copy) used in authentication
  copy:
    src: '{{ item.src }}'
    dest: '{{ item.dest }}'
  loop:
  - { src: nsswitch.conf,   dest: /etc/ }
  - { src: password-auth,   dest: /etc/pam.d/ }
  - { src: ssh_config,      dest: /etc/ssh/ }
  - { src: su,              dest: /etc/pam.d/ }
  - { src: system-auth-acc, dest: /etc/pam.d/ }

- name: Create symlink to system-auth-acc
  file:
    path: /etc/pam.d/system-auth
    state: link
    force: true
    src: system-auth-acc

- name: Configure files (template) used in authentication
  template:
    src: '{{ item.src }}'
    dest: '{{ item.dest }}'
    mode: "{{ item.mode|default('0644') }}"
  register: r_configure
  loop:
  - { src: krb5.conf.j2,     dest: /etc/krb5.conf }
  - { src: openldap.conf.j2, dest: /etc/openldap/ldap.conf }
  - { src: sshd_config.j2,   dest: /etc/ssh/sshd_config }               # list index: 2
  - { src: sssd.conf.j2,     dest: /etc/sssd/sssd.conf, mode: '0600' }  # list index: 3

# Configure sshd

- name: Create missing ssh host keys
  command: ssh-keygen -A
  register: r_ssh_keygen
  changed_when: r_ssh_keygen.stdout != ''

- name: Avoid Redhat extra comments when ssh'ing
  file:
    path: '{{ item }}'
    state: absent
  loop:
  - /etc/issue.d/cockpit.issue
  - /etc/motd.d/cockpit

- name: Restart sshd service
  systemd:
    name: sshd.service
    state: restarted
  when: r_configure.results[2].changed # list index: 2

# Configure sssd

- name: Check /etc/krb5.keytab
  stat:
    path: /etc/krb5.keytab
  register: r_keytab

- name: Delete Kerberos cache, get new keytab and wait 30s
  shell: kdestroy -c /tmp/krb5cc_host; cern-get-keytab; sleep 30
  when: not r_keytab.stat.exists

- name: Restart sssd service
  systemd:
    name: sssd.service
    state: restarted
  when: r_configure.results[3].changed or not r_keytab.stat.exists # list index: 3

# Configure sudo

- name: Configure /etc/sudoers
  lineinfile:
    path: /etc/sudoers
    regexp: '^Defaults[ \t]+requiretty'
    state: absent
